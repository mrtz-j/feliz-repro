import react from "@vitejs/plugin-react";
import { defineConfig } from "vite";
import { VitePWA } from "vite-plugin-pwa";
import path from 'path'
import { dependencies } from './package.json'

const vendorDeps = ['react', 'react-dom']

const chunksFromDeps = (deps: typeof dependencies, vendorDeps: string[]) => {
  const chunks: { [key: string]: string[] } = {}
  Object.keys(deps).forEach((key: string) => {
    if (vendorDeps.includes(key) || key.startsWith('@mui')) {
      return
    }
    chunks[key] = [key]
  })
  return chunks
}

export default defineConfig({
  // base: "./",
  root: ".",
  publicDir: "../public",
  build: {
    outDir: "../deploy",
    emptyOutDir: true,
    rollupOptions: {
      input: {
        main: path.resolve(__dirname, "./src/index.html")
      },
      output: {
        manualChunks: {
          vendor: vendorDeps,
          ...chunksFromDeps(dependencies, vendorDeps)
        },
        entryFileNames: 'js/main.min.js',
        chunkFileNames: 'js/[name].min.js',
        assetFileNames: '[ext]/[name].[ext]'
      },
    }
  },
  // server: {
  //   watch: {
  //     ignored: [
  //       "**/*.fs" // Don't watch F# files
  //     ]
  //   },
  //   host: '0.0.0.0',
  //   port: 8080,
  //   open: true
  // },
  resolve: {
    alias: {
      "rollup-plugin-terser": "@rollup/plugin-terser",
    },
  },
  plugins: [
    react(),
    VitePWA({
      registerType: "autoUpdate",
      includeAssets: ["favicon.svg"],
      manifest: {
        name: "Vite Mui React Typescript",
        short_name: "Vite Mui React Typescript",
        start_url: ".",
        display: "standalone",
        orientation: "portrait",
        theme_color: "#ffffff",
        icons: [
          {
            src: "pwa-192x192.png", // <== don't add slash, for testing
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "/pwa-512x512.png", // <== don't remove slash, for testing
            sizes: "512x512",
            type: "image/png"
          },
          {
            src: "pwa-512x512.png", // <== don't add slash, for testing
            sizes: "512x512",
            type: "image/png",
            purpose: "any maskable"
          }
        ]
      },
    })
  ]
});
