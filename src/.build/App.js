import { Union } from "./fable_modules/fable-library.4.1.4/Types.js";
import { union_type } from "./fable_modules/fable-library.4.1.4/Reflection.js";
import { createElement } from "react";
import React from "react";
import { createObj } from "./fable_modules/fable-library.4.1.4/Util.js";
import { singleton, ofArray } from "./fable_modules/fable-library.4.1.4/List.js";
import { Interop_reactApi } from "./fable_modules/Feliz.2.6.0/Interop.fs.js";
import { createRoot } from "react-dom/client";

export class Theme extends Union {
    constructor(tag, fields) {
        super();
        this.tag = tag;
        this.fields = fields;
    }
    cases() {
        return ["Light", "Dark"];
    }
}

export function Theme_$reflection() {
    return union_type("App.Theme", [], Theme, () => [[], []]);
}

export function App() {
    let elems;
    return createElement("div", createObj(singleton((elems = [createElement("span", {
        style: createObj(ofArray([["color", "#000000"], (() => {
            throw 1;
        })()])),
        children: "Hello World!",
    })], ["children", Interop_reactApi.Children.toArray(Array.from(elems))]))));
}

export const root = createRoot(document.getElementById("root"));

root.render(createElement(App, null));

//# sourceMappingURL=App.js.map
