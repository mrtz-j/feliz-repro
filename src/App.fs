module App

open Feliz
open Feliz.MaterialUI

type Theme = Light | Dark
        
module Theme =
    let defaultTheme = Styles.createTheme ()
    let light =
        Styles.createTheme [
                theme.palette.mode.light
                theme.palette.background.default' "red"
        ]
    
    let dark =
        Styles.createTheme [
                theme.palette.mode.dark
                theme.palette.background.default' "blue"
        ]

[<ReactComponent>]
let App() =
    let prefersDarkMode = Hooks.useMediaQuery("(prefers-color-scheme: dark)")
    let systemThemeMode = if prefersDarkMode then Dark else Light
    
    // Render
    Html.div [
        prop.children [
            Mui.themeProvider [
                themeProvider.theme (
                    match systemThemeMode with
                    | Dark -> Theme.dark
                    | Light -> Theme.light
                )
                themeProvider.children [
                    Mui.cssBaseline []
                    Html.span [
                        prop.style [
                            style.color.black
                            style.fontSize (length.px 72)
                        ]
                        prop.text "Hello World!"
                    ]
                    Mui.button [
                        button.centerRipple true
                        prop.text "Click me!"
                    ]
                ]
            ]
        ]
    ]
    |> fun x -> React.strictMode [x]
    

open Browser

let root = ReactDOM.createRoot(document.getElementById "root")
root.render(App())
