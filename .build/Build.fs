module Build

open Fake.Core
open Fake.IO
open Farmer
open Farmer.Builders

open Helpers

initializeContext()

// Path to folder containing Server.fsproj
// let serverPath = Path.getFullName "src/Server"
// Path to folder containing Client.fsproj
let clientPath = Path.getFullName "src"
// Folder where test scripts and project can be placed
let testPath = "tests"

// Where the bundle output should be dumped
let deployPath = Path.getFullName "deploy"

// Run cleanup
Target.create "Clean" (fun _ -> Shell.cleanDir deployPath)

// restore dotnet tools, if
Target.create "InstallClient" (fun _ ->
    run pnpm "install" "."
    run dotnet "tool restore" "."
)

Target.create "Format" (fun _ ->
    run dotnet "fantomas src/Client/ -r" "."
)

// Start process which publishes the client project into js-files for production
Target.create "Bundle" (fun _ ->
    // run dotnet (sprintf "publish -c Release -o \"%s\"" deployDir) serverPath
    
    Shell.mkdir "./deploy/public"
    run dotnet "fable --noCache -s -o .build --run vite build -c ../vite.config.ts" clientPath
    
    // Shell.cp_r "./src/Client/dist" "./deploy/public"
)


// Start process which publishes the server project into binaries for debug
Target.create "BundleDebug" (fun _ ->
    // run dotnet (sprintf "publish -c Release -o \"%s\"" deployDir) serverPath
    
    Shell.mkdir "./deploy/public"
    
    run dotnet "fable --noCache -s -o .build --run vite build --mode production -c ../vite.config.ts" clientPath
    
    // Shell.cp_r "./src/Client/dist" "./deploy/public"
)

// Start process for running the application while developing with hot-module reloading
Target.create "Run" (fun _ ->
    run dotnet "fable watch -s -o .build --run vite --mode development  -c ../vite.config.ts" clientPath
)

// if any test folders run tests
Target.create "Test" (fun _ ->
    if System.IO.Directory.Exists testPath then
      run dotnet "run" testPath
    else ()
)

open Fake.Core.TargetOperators

let dependencies = [
    "Clean"
        ==> "InstallClient"
        ==> "Bundle"

    "Clean"
        ==> "BundleDebug"

    "Clean"
        ==> "Test"

    "Clean"
        ==> "InstallClient"
        ==> "Run"
]

[<EntryPoint>]
let main args = runOrDefault args